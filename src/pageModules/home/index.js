import React from 'react';
import Header from '../../components/Header'
import Shows from '../../containers/Shows';
import "../styles.scss";

const Home = (props) => {
  return (
    <div className="home">
      <Header msg={'TV Shows List'} {...props} />
      <div className = "container">
        <Shows />
      </div>
    </div>
  )
}
export default Home;