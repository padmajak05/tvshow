import React from 'react';
import { shallow } from 'enzyme';
import Home from './index';
let wrapper = shallow(<Home />);

describe('ShowDetails Container Component', () => {
    
  it('renders the container', () => { 
    expect(wrapper.exists('.home')).toBe(true);
  });

  it('renders the list length', () => {
    expect(wrapper).toMatchSnapshot();
  });
});