import React from 'react';
import PropTypes from 'prop-types';
import Header from '../../components/Header'
import Details from '../../containers/Details'

const ShowDetails = (props) => {
    return (
        <div className="details">
            <Header msg={'TV Show Details'} {...props} isDetail={'detail'}/>
            <div className = "container">
                <Details id={props.match.params.id}/>
            </div>
        </div>
    )
}

ShowDetails.propTypes = {
   match: PropTypes.object

  };
  
  ShowDetails.defaultProps = {
    match: {}
  };  

export default ShowDetails;