import React from 'react';
import { shallow } from 'enzyme';
import ShowDetails from './index';

let props = {
  match: {
    params:{
      id: 1
    }
  }
}
let wrapper = shallow(<ShowDetails {...props}/>);

describe('showDetails Page Component', () => {
  it('renders the component', () => { 
    expect(wrapper.exists('.container')).toBe(true);
  });

  it('renders the details length', () => {
    expect(wrapper).toMatchSnapshot();
  });
});