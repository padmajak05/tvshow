import Details from './index';
import {shallow} from 'enzyme';
import React from 'react';
  
import checkPropTypes from 'check-prop-types';
import { applyMiddleware, createStore } from 'redux';
import rootReducer from '../../modules/Reducers';
import ReduxThunk from 'redux-thunk';

const middlewares = [ReduxThunk]

export const checkProps = (component, expectedProps) => {
    const propsErr = checkPropTypes(component.propTypes, expectedProps, 'props', component.name);
    return propsErr;
};

export const testStore = (initialState) => {
    const createStoreWithMiddleware = applyMiddleware(...middlewares)(createStore);
    return createStoreWithMiddleware(rootReducer, initialState);
};

const setUp = (initialState={}) =>  {
  const store = testStore(initialState);
  const wrapper = shallow(<Details store = {store} />).childAt(0).dive();
  return wrapper;
};

describe('Details Component', () =>{
  let wrapper;
  beforeEach(()=>{
    const initialState = {
      
    }
    wrapper = setUp(initialState);
  });

  it('Should render without errors', ()=>{
    expect(wrapper.length).toBe(1);
  });

  // it('Should Render a wrapper', () => {
  //   expect(wrapper.find('detail-wrapper').length).toBeTruthy();
  // });

  it('renders the component', () => {
    expect(wrapper).toMatchSnapshot();
  });
})