import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types';
import * as showAction from '../../modules/actionCreators/showsAction'
import Detail from '../../components/Detail'
import './styles.scss'

class Details extends React.Component {

  componentDidMount() {
    this.props.actions.getShowDetailsById(this.props.id)
  }

  render() {
    return (
      <div className="detail-wrapper" data-test="appComponent">
        <div className="row">
            {this.props.showData &&
              <Detail {...this.props.showData}/>
            }
          </div>
      </div> 
    )
  }
} 

const mapStateToProps = (state) => {
  return {
      showData: state.showData.payLoad
    }
  };
  
  const mapDispatchToProps = (dispatch) => {
    return {
      actions: bindActionCreators(showAction, dispatch)
    }
  };

  Details.propTypes = {
    actions: PropTypes.object,
    showData: PropTypes.object
  };

  Details.defaultProps = {
    actions: {},
    showData: {},
  };

export default connect(mapStateToProps, mapDispatchToProps)(Details);