import Shows from './index';
import {shallow} from 'enzyme';
import React from 'react';
  
import checkPropTypes from 'check-prop-types';
import { applyMiddleware, createStore } from 'redux';
import rootReducer from '../../modules/Reducers';
import ReduxThunk from 'redux-thunk';
const middlewares = [ReduxThunk]

export const checkProps = (component, expectedProps) => {
    const propsErr = checkPropTypes(component.propTypes, expectedProps, 'props', component.name);
    return propsErr;
};

export const testStore = (initialState) => {
    const createStoreWithMiddleware = applyMiddleware(...middlewares)(createStore);
    return createStoreWithMiddleware(rootReducer, initialState);
};

const setUp = (initialState={}) =>  {
  const store = testStore(initialState);
  const wrapper = shallow(<Shows store = {store} />).childAt(0).dive();
  return wrapper;
};

describe('Shows Component', () =>{
  let wrapper;
  beforeEach(()=>{
    const initialState = {
      
    }
    wrapper = setUp(initialState);
  });
  let ev = {
    target: {
      value: 'Action'
    }
  }
  it('Should render without errors', ()=>{
    expect(wrapper.length).toBe(1);
  });

  it('Should Render a wrapper', () => {
    expect(wrapper.find('wrapper').length).toBeDefined();
  });
  
  it("Expects to run onClick function when arrow is pressed in the DOM", () => {
    const showScroll = jest.fn();
    wrapper.find('.carousel__arrow--left').simulate('click');
    expect(showScroll.mock.calls.length).toEqual(0);
  });

  it('renders the component', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("Expects to find button HTML element in the DOM", () => {
    expect(wrapper.find('.carousel__arrow')).toHaveLength(2);
  });

  it('should set the passed selectedGenre to state', () => {
    expect(wrapper.instance().state.selectedGenre).toBe('Drama');
  });

  it("Expects to run onClick function when arrow is pressed in the DOM", () => {
    const showScroll = jest.fn();
    wrapper.find('.carousel__arrow--right').simulate('click');
    expect(showScroll.mock.calls.length).toBeDefined();
  });

  it('should update state.selectedGenre by data', () => {
    const instance = wrapper.instance();
    instance.handleClick(ev);
    expect(instance.state.selectedGenre).toBe('Action');
  });

  it('should set the passed selectedGenre to state', () => {
    expect(wrapper.instance().state.loading).toBe(true);
    wrapper.setState({ loading: false });
    expect(wrapper.instance().state.loading).toBe(false);
  });

})