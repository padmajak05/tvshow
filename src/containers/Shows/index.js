import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types';
import * as showsAction from '../../modules/actionCreators/showsAction'
import Show from '../../components/Show';
import SelectOption from '../../components/SelectOption';
import Spinner from '../../components/Spinner';
import './styles.scss';

class Shows extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      selectedGenre : 'Drama',
      options: ['Drama','Action', 'Comedy', 'Thriller'],
      topRating: 8.7,
      currentIndex: 0,
      styleDrag: "0px",
      loading: true
    }
  }

  //renders data based on genre
  getGenreData = () => {
    return this.props.data.filter((show)=>{
      return show.genres.toString().includes(this.state.selectedGenre);
    }).map((el,index)=>{
      return <Show {...el}  key={index}/>
    })
  }

  //renders top rated data
  getTopData = () => {
    return this.props.data.filter((el) => {
      return el.rating.average > this.state.topRating
    }).map((val,index) => {
      return <Show {...val} key={index} />
    })
  }

  componentDidMount(){
    this.props.actions.getShowsData()
    .then(()=>{
      this.setState({
        loading: false
      })
    })
  }

  handleClick=(ev)=>{
    this.setState ({
      selectedGenre: ev.target.value
    })
  }

  showScroll = (index) => {
    let leftVal = `calc(-${index} * 310px)`
    this.setState({
      styleDrag: `${leftVal}`,
      currentIndex: index,
    });
  }

  render() {
    return (
      <div className="wrapper">
        <div className = "spinner">
        <div className = {`${this.state.loading ? 'loading' : 'loaded'}`}>
            <Spinner />
        </div>
        </div>
        <div className={`${this.state.loading ? 'loaded' : 'loading'}`} >
        <h4>Top Rated Shows</h4>
        <div className="container">
          {/* eslint-disable no-unused-vars */}
          <div
            name="prev"
            className="carousel__arrow carousel__arrow--left"
            onClick={e => this.showScroll(this.state.currentIndex - 1)}>
            <span className="fa fa-2x fa-angle-left" />
          </div>
            <div className="row">
            <div className="carousel-slider">
              <div className="carousel-list draggable" >
                <div className="carousel-track" 
                  style={this.props.data && { "marginLeft": this.state.styleDrag} }>
                { this.props.data &&
                  this.getTopData()
                }
                </div>
              </div>
              </div>
            </div>
            <div
              className="carousel__arrow carousel__arrow--right"
              onClick={e => this.showScroll(this.state.currentIndex + 1)}>
              <span className="fa fa-2x fa-angle-right" />
            </div>
          </div>
          
          <div className="select">
            <h4>Select your Genre</h4>
            <select onChange={this.handleClick}>
              <SelectOption options={this.state.options} />
            </select>
          </div>
          <div className="row"> 
          { this.props.data &&
            this.getGenreData()
          }
          </div>
        </div>
      </div> 
    )
  }
} 

const mapStateToProps = (state) => {
  return {
      data: state.showsData.payLoad
    }
  };
  
  const mapDispatchToProps = (dispatch) => {
    return {
      actions: bindActionCreators(showsAction, dispatch) 
    }
  };

  Shows.propTypes = {
    data: PropTypes.array,
    actions: PropTypes.object,
  };

export default connect(mapStateToProps, mapDispatchToProps)(Shows);