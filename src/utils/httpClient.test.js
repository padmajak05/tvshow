import axios from 'axios';
import HttpClient from './httpClient';

// Mock module with Jest mock functions
//jest.mock('./httpClient');

describe('In HttpClient', () => {
  const baseURL = 'http://api.tvmaze.com';

  it('HttpClient to be defined', () => {
    expect(HttpClient).toBeDefined();
  });

  it('axios.create sets the baseUrl', () => {
    const expected = axios.create({
      baseURL,
    });
    expect(HttpClient.baseURL).toEqual(expected.baseURL);
  });
});
