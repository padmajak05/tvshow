import axios from 'axios';

axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded' // Header

const endpoint='http://api.tvmaze.com'
function http(method, obj) {
  
  const config = {
    method,
    url: obj.path,
    baseURL: endpoint
  };
  return axios(config);
}

const HttpClient = {
  get(obj = {}) {
    return http('GET', obj);
  },
};

export default HttpClient;
