import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import {applyMiddleware, createStore} from 'redux';
import reducer from '../src/modules/Reducers';
import { BrowserRouter } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';  
import Routes from "./routers";

const store = createStore(reducer, applyMiddleware(thunk));
const App = () => {
  return <Provider store={store}><BrowserRouter><Routes /></BrowserRouter></Provider>;
};

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

