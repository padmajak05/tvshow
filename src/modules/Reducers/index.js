import { combineReducers } from 'redux';
import showsReducer from './showsReducer';
import showReducer from './showReducer';

const rootReducer = combineReducers(
  {
  showsData: showsReducer,
  showData: showReducer,
});

export default rootReducer;