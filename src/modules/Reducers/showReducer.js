import {
  types
} from "../actionTypes";
   
  const initialState = {
    showData: null
  };
   
  export default function (state = initialState, action) {
    switch (action.type) {
      case types.TVSHOW_SUCCESS:
        return Object.assign({}, state, action);
      default:
        return state;
    }
   }