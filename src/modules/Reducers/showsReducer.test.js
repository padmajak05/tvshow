import {
    types
} from "../actionTypes";

import showsReducer from './showsReducer';  

describe('TV Shows Reducer', ()=>{
    it('Should return default state', () => {
    const newState = showsReducer(undefined, {});
    expect(newState).toEqual({"showsData":null})
    });

    it('Should return new state if receiving type', () => {
        const result = { showsData: null, type: 'TVSHOWS_SUCCESS', payLoad: {}}
        const data = {}
        const newState = showsReducer(undefined, {
            type: types.TVSHOWS_SUCCESS,
            payLoad: data
        });
        expect(newState).toEqual(result);
    });
});
