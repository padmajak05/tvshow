import {
  types
} from "../actionTypes";
   
  const initialState = {
    showsData: null
  };
   
  export default function (state = initialState, action) {
    switch (action.type) {
      case types.TVSHOWS_SUCCESS:
        return Object.assign({}, state, action);
      default:
        return state;
    }
   }