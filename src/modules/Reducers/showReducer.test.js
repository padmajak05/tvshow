import {
    types
} from "../actionTypes";

import showReducer from './showReducer';  

describe('TV Show Reducer', ()=>{
    it('Should return default state', () => {
    const newState = showReducer(undefined, {});
    expect(newState).toEqual({"showData":null})
    });

    it('Should return new state if receiving type', () => {
        const result = { showData: null, type: 'TVSHOW_SUCCESS', payLoad: {}}
        const data = {}
        const newState = showReducer(undefined, {
            type: types.TVSHOW_SUCCESS,
            payLoad: data
        });
        expect(newState).toEqual(result);
    });
});
