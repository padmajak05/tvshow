import {types} from '../actionTypes';
import HttpClient from '../../utils/httpClient';
import sortTvSHow from '../../sortShows';

export const getShowsData = () => (dispatch) => {
   return HttpClient.get({
        path: `/shows`,
    })
    .then((data) => {
      const payLoad = data.data;
      payLoad.sort(sortTvSHow);
      dispatch({
        type: types.TVSHOWS_SUCCESS, payLoad
      });
    })
    .catch(error => Promise.reject(error.response.data.message));
  };

  export const getShowDetailsById = (id) => (dispatch) => {
    return HttpClient.get({
      path: `/shows/${id}?embed=cast`,
    })
    .then((res) => {
      const payLoad = res.data;
      dispatch({
        type: types.TVSHOW_SUCCESS, payLoad
      });
    })
    .catch(error => Promise.reject(error.response.data.message));
  };

  export function getSearchResult (query) {
    return HttpClient.get({
      path: `/search/shows?q=${query}`,
     })
    .then((res) => {
      return res.data;
    })
    .catch(error => {
      throw(error);
    });
  }