import expect from 'expect';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import moxios from 'moxios';
import { getShowsData, getShowDetailsById, getSearchResult } from './showsAction';
import { TVSHOWS_SUCCESS, TVSHOW_SUCCESS } from '../actionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
window.localStorage = {};

describe('Auth actions', () => {
  
  beforeEach(() => moxios.install());
  afterEach(() => moxios.uninstall());

  it('creates TVSHOWS_SUCCESS when fetchData action is successful', (done) => {
    moxios.stubRequest('/shows', {
      status: 201
    });
    const expectedActions = [{ type: TVSHOWS_SUCCESS,
      payLoad: []}];
    const store = mockStore({});
    store.dispatch(getShowsData())
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    done();
  });

  it('creates TVSHOW_SUCCESS when fetchData action is successful', (done) => {
    moxios.stubRequest('/shows/1?embed=cast', {
      status: 201
    });
    const expectedActions = [{ type: TVSHOW_SUCCESS,
      payLoad: {}}];
    const store = mockStore({});
    store.dispatch(getShowDetailsById('1'))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    done();
  });

  it('fetches successfully data from an API', () => {
    const data = {};
    moxios.stubRequest('/search/shows?q=Drama', {
        status: 201
    });
    expect(getSearchResult()
    .then((res) => res.toEqual(data)
    ))
  });
  
  it('fetches successfully data from an API', () => {
    moxios.stubRequest('/search1/shows?q=Drama', {
        status: 404
    });
    expect(getSearchResult()
    .catch((error) => error.toThrow())
    )
  });

});
