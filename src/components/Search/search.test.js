import React from 'react';
import { shallow } from 'enzyme';
import Search from './index';
import {getSearchResult} from '../../modules/actionCreators/showsAction'
let wrapper = shallow(<Search />);

describe('Search Component', () => {
  it('renders the detail page', () => { 
    expect(wrapper.exists('.search-bar-container')).toBe(true);
  });

  it('renders the search length', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('renders the list length', () => {
    expect(wrapper.find('li').length).toBe(0)
  });

  it('calling handleChange', async() => {
    wrapper.handleChange = jest.fn();
    wrapper.handleChange.call(wrapper);
    expect(wrapper.handleChange).toBeCalled();
  });

  it('should update state.searchKey by value', () => {
    let data = ['a'];
    wrapper.getSearchResult = jest.fn();
    expect(wrapper.state()).toEqual({ searchKey: [] });
    expect(getSearchResult()
    .then((res) => res.toEqual(data)
    ))
  });

});
