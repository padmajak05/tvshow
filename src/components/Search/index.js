import React from 'react';
import PropTypes from 'prop-types';
import * as showAction from '../../modules/actionCreators/showsAction'

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchKey: []
        }
  }

  handleChange = (ev) => {
    showAction.getSearchResult(ev.target.value)
    .then((res)=>{
        this.setState({
            searchKey : res
        })
    });
  }

  render() {
    return (
        <div className="search-bar-container">
            <div className="search-bar">
                <input type="text" placeholder="Search" onKeyDown={this.handleChange} />
                { this.state.searchKey.length >= 1 &&
                    <ul className="list">
                        {this.state.searchKey.map((el,i)=>{
                            return (
                            <li key={i} onClick={() => { 
                                this.props.history.push(`/details/${el.show.id}`) 
                            }}>
                                {el.show.name}
                            </li>)
                        })}
                    </ul>
                }  
            </div>
        </div> 
    )
  }
}

Search.propTypes = {
    history: PropTypes.object
};

Search.defaultProps = {
    history: {}
};
export default Search;