import React from 'react';
import { shallow } from 'enzyme';
import Header from './index';
let wrapper = shallow(<Header/>);

describe('Header Component', () => {
  it('renders the header nav', () => { 
    expect(wrapper.exists('.header')).toBe(true);
  });
  
  it('renders the list length', () => {
    expect(wrapper).toMatchSnapshot();
  });
});