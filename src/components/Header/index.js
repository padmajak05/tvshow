import React from 'react';
import PropTypes from 'prop-types';
import Search from '../Search';
import {Link} from 'react-router-dom'
import './styles.scss'

const Header = (props) => {
  return (
    <div>
      <div className="header">
        <h1>{props.msg}</h1>
        {props.isDetail && <Link to={"/tvshows"} activeClassName="active">Go to Home</Link>}
      </div>
      {!props.isDetail && <Search {...props}/>}
    </div>
  )
}

Header.propTypes = {
  msg: PropTypes.string,
  isDetail: PropTypes.string,
};

Header.defaultProps = {
  msg: '',
  isDetail: '',
};

export default Header;