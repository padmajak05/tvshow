import React from 'react';

const Spinner = () => {
    return(
        <div className="text-center">
            <div className="spinner-border text-primary" role="status">
                <span className="sr-only">Loading...</span>
            </div>
            <h4 className ="text-primary">Loading....</h4>
        </div>
    )
}

export default Spinner;