import React from 'react';
import { shallow } from 'enzyme';
import Spinner from './index';
let wrapper = shallow(<Spinner/>);

describe('Spinner Component', () => {
  it('renders the header nav', () => { 
    expect(wrapper.exists('.text-center')).toBe(true);
  });
  
  it('renders the spinner length', () => {
    expect(wrapper).toMatchSnapshot();
  });
});