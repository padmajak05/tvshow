import React from 'react';
import { shallow } from 'enzyme';
import SelectOption from './index';

let props = {
  options: ['Drama','Action', 'Comedy', 'Thriller']
}
let wrapper = shallow(<SelectOption {...props}/>);

describe('SelectOption Component', () => {
  it('renders the detail page', () => { 
    expect(wrapper.exists('option')).toBe(true);
  });

  it('renders the search length', () => {
    expect(wrapper).toMatchSnapshot();
  });

});