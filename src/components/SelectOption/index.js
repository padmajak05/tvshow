import React from 'react';
import PropTypes from 'prop-types';

const SelectOption = (props) => {
  return (
    <>
        {props.options.map((item,i)=>{
            return( 
            <option key={i}>{item}</option>)
        })}
    </>
  )
}

SelectOption.propTypes = {
  items: PropTypes.array,
  options: PropTypes.array
};

SelectOption.defaultProps = {
  items: [{
    url: '',
    title: '',
    options: []
  }]
};

export default SelectOption;