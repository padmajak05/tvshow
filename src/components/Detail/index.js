import React from 'react';
import PropTypes from 'prop-types';

const Detail = (props) => {
  let {
    image, 
    name, 
    rating, 
    summary, 
    language, 
    officialSite, 
    premiered, 
    runtime, 
    status,
    genres
  } = props;
 
  return (
    <div className="card mb-3">
      <div className="row no-gutters">
        <div className="col-md-4">
          {image ? <img src={image.medium} className="card-img" alt="" /> : 
          <div className="no-image">No Image Available</div>}
        </div>
        <div className="col-md-8">
          <div className="card-body">
              <h5 className="card-title">{name}</h5>
              <p className="card-text">
                  <small className="text-muted">
                    {rating.average ? `${rating.average}/10` : 'N/A'}
                  </small>
              </p>
              <p className="card-text">
                <span><b>Summary: </b></span>
                <span dangerouslySetInnerHTML={{ __html: summary }} />
              </p>
              <div className="showDetails">
                <p>
                  <span>Language:</span>
                  <span>{language }</span>
                </p>
                <p><span>Genre: </span>
                  {genres && genres.map((el,i,arr) => {
                    return <span key={i}>
                    {`${  el} ${(arr.length - 1 !== i) ? '|' : ''}` }
                    </span>
                  })}
                </p>
                <p>
                  <span>Official Site:</span>
                  <span><a href={officialSite} alt="" target="_blank" rel="noopener noreferrer">
                    {officialSite ? officialSite : 'NA'}</a>
                  </span>
                </p>
                <p>
                  <span>Premiered:</span>
                  <span>{premiered ? premiered : 'NA'}</span>
                </p>
                <p>
                  <span>Runtime:</span>
                  <span>{runtime }</span>
                </p>
                <p>
                  <span>Status:</span>
                  <span>{ status }</span>
                </p>
              </div>
          </div>
        </div>
      </div>
    </div> 
  )
} 

Detail.propTypes = {
  image: PropTypes.object,
  name: PropTypes.string,
  rating: PropTypes.object,
  summary: PropTypes.string,
  language: PropTypes.string,
  officialSite: PropTypes.string,
  premiered: PropTypes.string,
  runtime: PropTypes.number,
  status: PropTypes.string,
  genres: PropTypes.array
};

Detail.defaultProps = {
  image: {},
  name: '',
  rating: {},
  summary: '',
  language: '',
  officialSite: '',
  premiered: '',
  status: '',
  genres: []
};

export default Detail;