import React from 'react';
import { shallow } from 'enzyme';
import Details from './index';
let props = {
  "image": {
    "medium": "http://static.tvmaze.com/uploads/images/medium_portrait/81/202627.jpg",
  },
  "rating": {
    "average": 6.5
  }
}
let wrapper = shallow(<Details {...props}/>);

describe('Detail Component', () => {
  it('renders the detail page', () => { 
    expect(wrapper.exists('.card')).toBe(true);
  });

  it('renders the detail length', () => {
    expect(wrapper).toMatchSnapshot();
  });
});