import React from 'react';
import { shallow } from 'enzyme';
import Show from './index';
let props = {
  "image": {
    "medium": "http://static.tvmaze.com/uploads/images/medium_portrait/81/202627.jpg",
  },
  "rating": {
    "average": 6.5
  },
  "premiered": "2013-06-24",
  "genres": [
    "Drama",
    "Science-Fiction",
    "Thriller"
  ]
}
let wrapper = shallow(<Show {...props} />);

describe('Show Component', () => {
  it('renders the detail page', () => { 
    expect(wrapper.exists('.col')).toBe(true);
  });

  it('renders the Show length', () => {
    expect(wrapper).toMatchSnapshot();
  });
});