import React from 'react';
import PropTypes from 'prop-types';

const Show = (props) => {
  let {id,rating, premiered, image, name, genres} = props;
  return (
    <div className="col">
      <a href={`/details/${id}`}>
        <div className="card">
        <img src={image.medium} className="card-img-top" alt="" />
        <div className="rating">{rating.average}</div>
          <div className="card-body">
              <p className="card-title">
                {name}
                <span> ({premiered.substring(0, 4) }) </span>
              </p>
              <ul className="seriesGenre">
                {genres.map((el,i,arr) => {
                  return <li key={i}>
                  {`${ el} ${(arr.length - 1 !== i) ? '|' : ''}` }
                  </li>
                })}
              </ul>
          </div>
        </div>
        </a>
    </div> 
  )
} 

Show.propTypes = {
  id: PropTypes.number,
  image: PropTypes.object,
  name: PropTypes.string,
  rating: PropTypes.object,
  premiered: PropTypes.string,
  genres: PropTypes.array
};

Show.defaultProps = {
  image: {},
  name: '',
  rating: {},
  premiered: '',
  genres: []
};

export default Show;