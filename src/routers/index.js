import { Switch, Route, Redirect } from "react-router-dom";

import React from 'react';
import Home from "../pageModules/home";
import ShowDetails from '../pageModules/showDetails';

const Routes = () => {
  return <Switch>
    <Route path="/" exact={true} render={() => <Redirect to="/tvshows" />} />
    <Route path="/tvshows" component={Home} />
    <Route path="/details/:id" component={ShowDetails} />
    <Route render={() => <Redirect to={{pathname: "/"}} />} />
  </Switch>
}

export default Routes;