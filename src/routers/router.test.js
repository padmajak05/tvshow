import React from 'react';
import { shallow } from 'enzyme';
import { MemoryRouter } from 'react-router';
import Home from "../pageModules/home";

test('invalid path should redirect to 404', () => {
  const wrapper = shallow(
    <MemoryRouter initialEntries={[ '/' ]}>
      <Home/>
    </MemoryRouter>
  );
  expect(wrapper.find(Home)).toHaveLength(1);
});

test('valid path should not redirect to 404', () => {
  const wrapper = shallow(
    <MemoryRouter initialEntries={[ '/tvshows' ]}>
      <Home/>
    </MemoryRouter>
  );
  expect(wrapper.find(Home)).toHaveLength(1);
});